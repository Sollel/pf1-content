_id: 1G3PcOFZhjf6HlnL
name: '- Change Formulas'
content: >-
  <blockquote>

  <p>Below are a sample of formulas used in ability, item, and feature Changes
  that may prove useful for adding or updating for functionality. While these
  are not comprehensive, they aim is to aid GM's and Players to correct or add
  missing changes.&nbsp; Thanks to @Eitan and various others on the Discord for
  their useful examples.</p>

  <p>&nbsp;</p>

  <p>Additionally, if you add Changes to any of the entities created by this
  module or the pf1 sys, please consider using Data Toolbox's Contribute button,
  to upload them to be added/merged in future releases.&nbsp;</p>

  </blockquote>

  <p>&nbsp;</p>

  <h1>'Change' Tips</h1>

  <ul>

  <li>Most values are listed via hovering over the appropriate value in the
  character sheet</li>

  <li>Ability modifier: @abilities.con.mod</li>

  <li>Ability modifier without temporary buffs (i.e. Rage):
  @abilities.con.baseMod</li>

  <li>Changes can be set to Add to the value (+) or Replace (=)</li>

  <li>Scripts using js can also be used for changes, for more complicated
  things.</li>

  <li>Not everything available in Pathfinder can currently be accomplished
  through changes</li>

  </ul>

  <p>&nbsp;</p>

  <h1>Math functions</h1>

  <h2>Basic</h2>

  <p><strong>Round Up</strong></p>

  <p><code>ceil()</code></p>

  <p><strong>Example:</strong> <code>ceil(5 / 2)</code></p>

  <p><strong>Result:</strong> 3</p>

  <hr />

  <p><strong>Round Down</strong></p>

  <p><code>floor()</code></p>

  <p><strong><code></code>Example:</strong> <code>floor(@attributes.bab.total /
  6)</code></p>

  <p><strong>Result:</strong> Divide by 6 and round down</p>

  <hr />

  <p><strong>Minimum</strong></p>

  <p><code>min(x, y)</code></p>

  <p><strong>Example:</strong> <code>min(5,@cl)d6</code></p>

  <p><strong>Result:</strong> Scales from caster level, up to a max value of
  5d6</p>

  <hr />

  <h3><strong>Maximum</strong></h3>

  <p style="font-size: 14px;"><code>max(x, y)&nbsp;</code></p>

  <p style="font-size: 14px;"><strong>Example:</strong>&nbsp;<code>max(4,
  (@classes.class.level))</code></p>

  <p style="font-size: 14px;"><strong>Result:</strong>&nbsp;Uses 4, unless class
  level is greater. Effectively creates a lower bounds until class level &gt; 4,
  then uses class levels.&nbsp;</p>

  <hr />

  <h3><strong>Conditional</strong></h3>

  <p><code>(X &gt; Y ? A : B)&nbsp;</code></p>

  <p><strong>Example:</strong>&nbsp;<code>@attributes.hd.total &gt; 9 ? 4 :
  2</code></p>

  <p><strong>Result:</strong> If HD is greater than 9, output is 4, if it is not
  then 2</p>

  <hr />

  <h3><strong>Absolute value</strong></h3>

  <p><code>abs()&nbsp;</code></p>

  <p><strong>Example:</strong>&nbsp;<code>abs(2 - 4)</code></p>

  <p><strong>Result:</strong>&nbsp;Outputs 2 (rather than -2)</p>

  <hr />

  <p>Foundry uses JS Math. For more math functions, go <a
  href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math">here</a>.
  Reading how JS handles math might make things easier to
  understand.&nbsp;&nbsp;</p>

  <p>&nbsp;</p>

  <h2>Combined examples</h2>

  <p><code></code><strong style="font-size: 1.125em;">Increase by 1 for every 4
  BAB</strong></p>

  <p><code>max(1, floor(@attributes.bab.total / 4)))</code></p>

  <p><strong>Result:</strong> BAB = 3: 1; BAB = 6: 1; BAB = 14: 3</p>

  <p>&nbsp;</p>

  <p><strong>Half Class level, minimum 1</strong></p>

  <p><code>max(1, floor(@classes.boo.level / 2))</code></p>

  <p>&nbsp;</p>

  <h1>Specific Use Examples</h1>

  <p>&nbsp;</p>

  <h2>Cavalier Challenge Uses</h2>

  <p><em>Once per day, plus one for every three levels beyond (1, 4, 7, 10,
  ...)</em></p>

  <ul>

  <li><strong>Limited Uses:</strong> <code>1 + floor((@classes.cavalier.level -
  1) / 3)</code></li>

  </ul>

  <p>&nbsp;</p>

  <h2>Channel Positive Energy</h2>

  <p><em>Heals 1d6 + 1d6 for every 2 levels beyond 1st (3, 5, 7, ...)</em></p>

  <ul>

  <li><strong>Healing:</strong><code> ceil(@classes.cleric.level /
  2))d6</code></li>

  </ul>

  <p><em>DC for the save is 10 + 1/2 cleric level + Cha mod</em></p>

  <ul>

  <li><strong>Save DC: </strong><code>10 + floor(@classes.cleric.level / 2) +
  @abilities.cha.mod</code></li>

  </ul>

  <p>&nbsp;</p>

  <h2>Cure Light Wounds</h2>

  <p><em>Heals 1d8 + cl (max of 5)</em></p>

  <ul>

  <li><strong>Healing:</strong><code> 1d8 + min(5, @cl)</code></li>

  </ul>

  <p>&nbsp;</p>

  <h2>Fireball</h2>

  <p><em>Deals 1d6 per caster level, max 10d6</em></p>

  <ul>

  <li><strong>Damage:</strong><code> (min(10, @cl))d6</code></li>

  </ul>

  <p>&nbsp;</p>

  <h2>Fleet</h2>

  <p><em>Increase movement speed by 5, while wearing light (1) or no armor (0)
  and not encumbered. (@armor.type - medium armor = 2, heavy = 3)</em></p>

  <ul>

  <li><strong>Change (+):</strong><code>&nbsp;(@armor.type &lt; 2 &amp;&amp;
  @attributes.encumbrance.level &lt; 1) ? 5 : 0</code></li>

  </ul>

  <p>&nbsp;</p>

  <h2>Kinetic Blasts</h2>

  <p><em>These are some of the more complex formula I've seen, that also include
  the use of resources. </em></p>

  <h3><strong>Elemental Overflow</strong> (added to attack bonus of blasts)</h3>

  <p><em>See @Compendium[pf1.class-abilities.NOhmhKgboJ9qdGvg]{Elemental
  Overflow} for description</em></p>

  <ul>

  <li><strong>Attack</strong><strong>:</strong>
  &nbsp;<code>min(@resources.burn.value, floor(@classes.kineticist.level /
  3))</code></li>

  </ul>

  <p><code></code></p>

  <p><code></code></p>

  <h3><strong>Energy Blast</strong></h3>

  <p><em>Deals 1d6 plus 1d6 every 2 levels beyond 1st (1, 3, 5, ...) + damage
  from Elemental Overflow</em></p>

  <ul>

  <li><strong>Attack:</strong> <code>min(@resources.burn.value,
  floor(@classes.kineticist.level / 3))</code></li>

  <li><strong>Damage:</strong> <code>(ceil(@classes.kineticist.level/2))d6
  </code><code>+ min(@resources.burn.value, floor(@classes.kineticist.level/3))
  * 2 * @critMult</code></li>

  </ul>

  <p><code></code></p>

  <p><code></code></p>

  <h3><strong>Physical Blast</strong></h3>

  <p><em>Deals 1d6+1, plus 1d6+1 every 2 levels beyond 1st (1, 3, 5, ...) +
  damage from Elemental Overflow</em></p>

  <ul>

  <li><code></code><strong>Attack:</strong>&nbsp;<code>min(@resources.burn.value,
  floor(@classes.kineticist.level / 3))</code></li>

  <li><strong>Damage: </strong><code>(ceil(@classes.kineticist.level/2))d6 +
  ceil(@classes.kineticist.level/2) + min(@resources.burn.value,
  floor(@classes.kineticist.level/3)) * 2 * @critMult</code></li>

  </ul>

  <p>&nbsp;</p>

  <h3><strong>Energy Composite Blasts</strong></h3>

  <ul>

  <li><strong>Attack:</strong> &nbsp;<code>min(@resources.burn.value,
  floor(@classes.kineticist.level / 3))</code></li>

  <li><strong>Damage: </strong><code>(2 * ceil(@classes.kineticist.level/2))d6 +
  floor(@abilities.con.mod/2) + min(@resources.burn.value,
  floor(@classes.kineticist.level/3)) * 2 * @critMult</code></li>

  </ul>

  <p>&nbsp;</p>

  <h2>Power Attack</h2>

  <p><em>Gives -1 Atk / +2 Dmg, increasing by -1/+2 when BAB = 4, and every 4
  points after</em></p>

  <ul>

  <li><strong>Attack:&nbsp;</strong><code>-floor((@attributes.bab.total+4)/4)</code></li>

  <li><strong>Damage:</strong><code>
  floor((@attributes.bab.total+4)/4)*2</code></li>

  </ul>

  <p>&nbsp;</p>

  <h2>Sneak Attack</h2>

  <p><em>Rogue sneak attack (Levels 1, 3, 5, ...)</em></p>

  <ul>

  <li><strong>Damage:</strong> <code>ceil(@classes.rogue.level /
  2)d6</code></li>

  </ul>

  <p>&nbsp;</p>

  <p><em>Slayer sneak attack (Levels 3, 6, 9, ...)</em></p>

  <ul>

  <li><strong>Damage:</strong> <code>floor(@classes.slayer.level /
  3)d6</code></li>

  </ul>

  <p>&nbsp;</p>

  <h2>Skill Focus (Perception)<span style="font-size:
  14px;"><em>&nbsp;</em></span></h2>

  <p><em>Gives +3 base, +6 at 10 ranks+&nbsp;</em></p>

  <ul>

  <li><strong>Change:</strong> @skills.per.rank &gt; 9 ? 6 : 3</li>

  </ul>

  <p><code></code></p>

  <p><code></code></p>

  <p><code></code></p>
img: icons/sundries/documents/blueprint-magical.webp
pages: []
_key: '!journal!1G3PcOFZhjf6HlnL'

