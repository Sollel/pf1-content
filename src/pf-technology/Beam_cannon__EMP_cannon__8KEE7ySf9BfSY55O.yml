_id: 8KEE7ySf9BfSY55O
name: Beam cannon (EMP cannon)
type: weapon
img: icons/skills/ranged/cannon-barrel-firing-orange.webp
system:
  description:
    value: >-
      <h2>Statistics</h2><p><strong>Type</strong> two-handed ranged;
      <strong>Proficiency</strong> exotic (heavy
      weaponry)</p><p><strong>Damage</strong> Special (small), Special (medium);
      <strong>Damage Type</strong> Special; <strong>Critical</strong>
      x2</p><p><strong>Range</strong> 300 ft.; <strong>Capacity</strong> 50;
      <strong>Usage</strong> 1 charge; <strong>Special</strong> Automatic,
      touch</p><h2>Description</h2><p>A beam cannon is an immense version of one
      of the six more common beam rifle types. Beam cannons are generally
      referred to by type as summarized below. In addition to the standard
      automatic firing mode options (<em>Pathfinder Campaign Setting: Technology
      Guide</em> 20), a beam cannon can be used to launch a devastating blast of
      energy. This is a full-round action, and these blasts can't be used to
      make iterative attacks. When a beam cannon is used to launch a blast, no
      attack roll is made-you simply target a 20-foot-radius spread within a
      maximum range of 300 feet. You must have line of effect to your target
      when you launch a blast; if the blast impacts a solid object before
      reaching the target area, it explodes at that point. All creatures caught
      in the blast radius take 6d6 points of damage of the energy type
      appropriate to the cannon (Reflex DC 14 half). Each blast also has an
      additional effect as determined by the type of cannon. Firing a blast from
      a beam cannon consumes 10 charges.</p><p><strong>EMP Cannon</strong>: An
      EMP cannon fires electromagnetic energy that cannot harm most living
      creatures. It deals 4d6 points of electricity damage against robots, and
      half as much damage against androids and creatures with cybernetic
      implants. A creature that takes damage from a critical hit by an EMP
      cannon must succeed at a DC 15 Fortitude save or be staggered for 1d4
      rounds. An EMP cannon's blast deals 8d6 points of electricity damage of
      the same electromagnetic nature. An EMP cannon costs 58,000 gp and has a
      Craft DC of 33.</p><h2>Construction</h2><p><strong>Craft</strong> DC 33;
      <strong>Cost</strong> 29,000 gp</p><p>Craft Technological Arms and Armor,
      military lab</p>
    unidentified: ''
  tags: []
  quantity: 1
  weight:
    value: 18
  price: 58000
  size: med
  equipped: true
  resizing: false
  identified: true
  hp:
    max: 10
    value: 10
  broken: false
  hardness: 10
  carried: true
  unidentified:
    price: 0
    name: ''
  cl: 0
  aura:
    custom: false
    school: ''
  changes: []
  changeFlags:
    loseDexToAC: false
    noMediumEncumbrance: false
    noHeavyEncumbrance: false
    mediumArmorFullSpeed: false
    heavyArmorFullSpeed: false
  actions:
    - _id: u2mjm3hlbbakh33p
      name: Attack
      img: icons/skills/ranged/cannon-barrel-firing-orange.webp
      description: ''
      tag: beamCannonEmpCannon
      activation:
        cost: 1
        type: attack
        unchained:
          cost: 1
          type: attack
      duration:
        value: null
        units: inst
      target:
        value: ''
      range:
        value: '300'
        units: ft
        maxIncrements: 1
        minValue: null
        minUnits: ''
      uses:
        autoDeductChargesCost: ''
        self:
          value: 0
          maxFormula: ''
          per: ''
        per: null
        value: 0
        maxFormula: ''
        max: 0
      measureTemplate:
        type: ''
        size: ''
        overrideColor: false
        customColor: ''
        overrideTexture: false
        customTexture: ''
      attackName: ''
      actionType: rwak
      attackBonus: ''
      critConfirmBonus: ''
      damage:
        parts:
          - formula: sizeRoll(4, 6, @size)
            type:
              values:
                - electric
              custom: ''
        critParts: []
        nonCritParts: []
      attackParts: []
      formulaicAttacks:
        count:
          formula: ceil(@attributes.bab.total / 5) - 1
        bonus:
          formula: '@formulaicAttack * -5'
        label: ''
      formula: ''
      ability:
        attack: dex
        damage: ''
        damageMult: 1
        critRange: 20
        critMult: 2
      save:
        dc: ''
        type: ''
        description: ''
        harmless: true
      effectNotes:
        - >-
          A creature that takes damage from a critical hit by an EMP cannon must
          succeed at a DC 15 Fortitude save or be staggered for 1d4 rounds
      attackNotes: []
      soundEffect: ''
      powerAttack:
        multiplier: null
        damageBonus: 2
        critMultiplier: 1
      naturalAttack:
        primaryAttack: true
        secondary:
          attackBonus: '-5'
          damageMult: 0.5
      held: ''
      nonlethal: false
      touch: true
      usesAmmo: false
      spellEffect: ''
      spellArea: ''
      conditionals: []
      enh:
        value: null
      ammoType: none
      area: ''
      material:
        normal:
          value: ''
          custom: false
        addon:
          - null
          - null
      alignments:
        lawful: null
        chaotic: null
        good: null
        evil: null
    - _id: 62RRHArEVlPGFNK9
      name: Blast
      img: icons/skills/ranged/cannon-barrel-firing-orange.webp
      description: ''
      tag: ''
      activation:
        cost: 1
        type: full
        unchained:
          cost: 3
          type: action
      duration:
        value: null
        units: inst
      target:
        value: Robots, Androids or creatures with cybernetics
      range:
        value: '300'
        units: ft
        maxIncrements: 1
        minValue: null
        minUnits: ''
      uses:
        autoDeductChargesCost: '10'
        self:
          value: 0
          maxFormula: ''
          per: ''
      measureTemplate:
        type: circle
        size: '20'
        overrideColor: false
        customColor: ''
        overrideTexture: false
        customTexture: ''
      attackName: ''
      actionType: other
      attackBonus: ''
      critConfirmBonus: ''
      damage:
        parts:
          - formula: 8d6
            type:
              values:
                - electric
              custom: ''
        critParts: []
        nonCritParts: []
      attackParts: []
      formulaicAttacks:
        count:
          formula: ''
        bonus:
          formula: ''
        label: null
      formula: ''
      ability:
        attack: ''
        damage: ''
        damageMult: 1
        critRange: 20
        critMult: 2
      save:
        dc: '14'
        type: ref
        description: Reflex half
        harmless: false
      effectNotes:
        - Androids and creatures with cybernetics take half damage
      attackNotes: []
      soundEffect: ''
      powerAttack:
        multiplier: null
        damageBonus: 2
        critMultiplier: 1
      naturalAttack:
        primaryAttack: true
        secondary:
          attackBonus: '-5'
          damageMult: 0.5
      held: ''
      nonlethal: false
      touch: false
      ammoType: none
      spellEffect: ''
      area: 20 ft. radius
      conditionals: []
      enh:
        value: null
      material:
        normal:
          value: ''
          custom: false
        addon:
          - null
          - null
      alignments:
        lawful: null
        chaotic: null
        good: null
        evil: null
  uses:
    per: charges
    value: 50
    maxFormula: '50'
    autoDeductChargesCost: '1'
    max: 0
    pricePerUse: ''
  attackNotes: []
  effectNotes: []
  contextNotes: []
  links:
    children: []
  tag: ''
  flags:
    boolean: {}
    dictionary: {}
  scriptCalls: []
  masterwork: false
  enh: null
  proficient: true
  held: normal
  baseTypes:
    - Beam cannon (EMP cannon)
  weaponGroups:
    value:
      - firearms
    custom: ''
  material:
    base:
      value: steel
      custom: false
    normal:
      value: ''
      custom: false
    addon:
      - null
  subType: exotic
  weaponSubtype: ranged
  properties:
    ato: true
    blc: false
    brc: false
    dea: false
    dis: false
    dst: false
    dbl: false
    fin: false
    frg: false
    grp: false
    imp: false
    mnk: false
    nnl: false
    prf: false
    rch: false
    snd: false
    thr: false
    trp: false
    sct: false
    spc: false
  showInQuickbar: false
  ammoType: ''
  bonus:
    value: 0
    _deprecated: true
  damage2:
    value: ''
    _deprecated: true
  damage2Type:
    value: ''
    _deprecated: true
  source:
    id: PZO9089
    pages: '62'
  ammo:
    type: ''
  alignments:
    lawful: false
    chaotic: false
    good: false
    evil: false
  artifact: false
  timeworn: false
  cursed: false
ownership:
  default: 0
folder: 07LmOqf2qBzm5RsK
flags:
  pf1:
    migration: '9.5'
effects: []
sort: 0
_stats:
  systemId: pf1
  systemVersion: '9.6'
  coreVersion: '11.315'
  createdTime: null
  modifiedTime: 1703379046092
  lastModifiedBy: tvZNTwoQNC6fjdSZ
_key: '!items!8KEE7ySf9BfSY55O'

