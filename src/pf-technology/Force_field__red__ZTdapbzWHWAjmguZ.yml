_id: ZTdapbzWHWAjmguZ
name: Force field (red)
type: equipment
img: icons/magic/water/barrier-ice-shield.webp
system:
  description:
    value: >-
      <p><strong>Source</strong> <em>Technology Guide pg.
      45</em></p><h2>Statistics</h2></h2><p><strong>Slot</strong>
      wrist</p><p><strong>Capacity</strong> varies; <strong>Usage</strong> 1
      charge/minute</p><h2>Description</h2><p>Force fields are powered by a
      surprisingly light pair of slender, silvery bracelets. When activated as a
      standard action, dozens of tiny biofeedback needles pierce the wearer's
      arms to interface with the nervous system; this process is somewhat
      painful and the wearer must succeed at a DC 15 Fortitude save to avoid
      being dazed for 1 round as the force field is activated. Once active, the
      device generates an invisible and intangible field of force around the
      target. The force field blocks solids and liquids, but not gases or light
      (including laser beams). The user of a force field can still breathe, but
      she cannot eat or drink.</p><p>Activating a force field is a standard
      action that consumes 1 charge, after which point the field consumes 1
      additional charge every minute it remains active. While a force field is
      active, the user gains a number of temporary hit points that varies
      depending on the force field's power. All damage dealt to the wearer of a
      force field is subtracted from the temporary hit points it grants first.
      As long as the force field is active, the wearer is immune to critical
      hits (but not precision-based damage, such as sneak attacks). A force
      field has fast healing that replenishes its temporary hit points at a
      fixed rate each round, but if the force field's temporary hit points are
      ever reduced to 0, the force field shuts down and cannot be reactivated
      for 24 hours. Force fields are automatically reduced to 0 hp by
      disintegration effects. A force field can be deactivated as a free
      action.</p><p>The charge capacity, amount of temporary hit points granted,
      and rate of fast healing these temporary hit points have varies according
      to the force field's color code, as detailed
      below.</p><table><tbody><tr><td><strong>Color</strong></td><td><strong>Capacity</strong></td><td><strong>Temp
      HP</strong></td><td><strong>Fast Healing</strong></td></tr>
      <tr><td>Brown</td><td>10</td><td>5</td><td>1</td></tr>
      <tr><td>Black</td><td>15</td><td>10</td><td>2</td></tr>
      <tr><td>White</td><td>20</td><td>15</td><td>3</td></tr>
      <tr><td>Gray</td><td>25</td><td>20</td><td>4</td></tr>
      <tr><td>Green</td><td>30</td><td>25</td><td>5</td></tr>
      <tr><td>Red</td><td>35</td><td>30</td><td>6</td></tr>
      <tr><td>Blue</td><td>40</td><td>35</td><td>7</td></tr>
      <tr><td>Orange</td><td>45</td><td>40</td><td>8</td></tr>
      <tr><td>Prismatic</td><td>50</td><td>60</td><td>10</td></tr></tbody></table><h2>Construction</h2><p><strong>Craft</strong>
      DC 32 (red); <strong>Cost</strong> 50,000 gp (red)</p><p>Craft
      Technological Item, graviton lab</p>
    unidentified: ''
  tags: []
  quantity: 1
  weight:
    value: 1
  price: 100000
  size: med
  equipped: true
  resizing: false
  identified: true
  hp:
    max: 10
    value: 10
  broken: false
  hardness: 0
  carried: true
  unidentified:
    price: 0
    name: ''
  cl: 0
  aura:
    custom: false
    school: ''
  changes: []
  changeFlags:
    loseDexToAC: false
    noEncumbrance: false
    mediumArmorFullSpeed: false
    heavyArmorFullSpeed: false
    noMediumEncumbrance: false
    noHeavyEncumbrance: false
  contextNotes: []
  actions:
    - _id: hl8wJx6KHpfMKx6B
      name: Use
      img: icons/magic/water/barrier-ice-shield.webp
      description: ''
      tag: ''
      activation:
        cost: 1
        type: ''
        unchained:
          cost: 1
          type: ''
      duration:
        value: '1'
        units: minute
      target:
        value: ''
      range:
        value: null
        units: personal
        maxIncrements: 1
        minValue: null
        minUnits: ''
      uses:
        autoDeductChargesCost: ''
        self:
          value: 0
          maxFormula: ''
          per: ''
      measureTemplate:
        type: ''
        size: ''
        overrideColor: false
        customColor: ''
        overrideTexture: false
        customTexture: ''
      attackName: ''
      actionType: save
      attackBonus: ''
      critConfirmBonus: ''
      damage:
        parts: []
        critParts: []
        nonCritParts: []
      attackParts: []
      formulaicAttacks:
        count:
          formula: ''
        bonus:
          formula: ''
        label: null
      formula: ''
      ability:
        attack: ''
        damage: ''
        damageMult: 1
        critRange: 20
        critMult: 2
      save:
        dc: '15'
        type: fort
        description: Fortitude negates
      effectNotes:
        - Creatures that fail the save are dazed for 1 round
      attackNotes: []
      soundEffect: ''
      powerAttack:
        multiplier: ''
        damageBonus: 2
        critMultiplier: 1
      naturalAttack:
        primaryAttack: true
        secondary:
          attackBonus: '-5'
          damageMult: 0.5
      nonlethal: false
      touch: false
      usesAmmo: false
      spellEffect: ''
      spellArea: ''
      conditionals: []
      enh:
        value: null
  uses:
    per: charges
    value: 35
    maxFormula: '35'
    autoDeductChargesCost: '1'
    max: 0
    pricePerUse: ''
  attackNotes: []
  effectNotes: []
  links:
    children: []
    charges: []
  tag: ''
  useCustomTag: false
  flags:
    boolean: {}
    dictionary: {}
  scriptCalls: []
  masterwork: false
  enh: null
  proficient: true
  held: normal
  subType: other
  baseTypes: []
  equipmentSubtype: lightArmor
  armor:
    value: 0
    dex: null
    acp: 0
    enh: 0
  spellFailure: 0
  slot: wrists
  showInQuickbar: false
  nonlethal: false
ownership:
  default: 0
folder: InUvu8pSIVYU8kHo
flags:
  pf1:
    migration: '9.5'
effects: []
_key: '!items!ZTdapbzWHWAjmguZ'

