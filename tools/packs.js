import { extractPack, compilePack } from "@foundryvtt/foundryvtt-cli";
import { promises as fs } from "fs";
import path from "path";
import PackGroup from "./lib/packgroup.js";
import url from "node:url";
import yargs from "yargs";

const __filename = url.fileURLToPath(import.meta.url);

// Only handle commands if this script was executed directly
console.log("argv is ", process.argv);
if (process.argv[1] === __filename || process.argv[2] === "pack") {
    yargs(process.argv.slice(2))
      .demandCommand(1, 1)
      .command({
        command: "pack",
        describe: `Compile all packs (or a pack) into ldb file(s)`,
        handler: async (argv) => {
          await compilePacks(argv.folder ?? process.argv[3] ?? null);
        },
      })
      .command({
        command: "unpack",
        describe: `Extract all packs (or a pack) into source JSONs`,
        handler: async (argv) => {
          await extractPacks(argv.folder ?? process.argv[3] ?? null);
        },
      })
      .command({
        command: "prune",
        describe: `Prune all packs (or a pack)`,
        handler: async (argv) => {
          await prunePacks(argv.folder ?? process.argv[3] ?? null);
        },
      })
      // Option to overwrite the default `reset` option
      .option("folder", { describe: "Work with a specific pack", type: "string" })
      .parse();
  }

async function extractPacks(pack) {
    const packs = pack ? [pack] : await fs.readdir(path.join("release", "packs"));
    for (const pack of packs) {
        if (pack === ".gitattributes") continue;
        console.log("Unpacking " + pack);
        const dest = path.join("src", pack);
        const src = path.join("release", "packs", pack);

        await extractPack(src, dest, { yaml: true, log: true, clean: true });
    }
}

async function compilePacks(pack) {
    const packs = pack ? [pack] : await fs.readdir(path.join("src"));
    for (const pack of packs) {
        if (pack === ".gitattributes") continue;
        console.log("Packing " + pack);
        const src = path.join("src", pack);
        const dest = path.join("release", "packs", pack);
        await compilePack(src, dest, { yaml: true, log: true});
    }
}

async function prunePacks(pack) {
    const folders = pack ? [pack] : await fs.readdir(path.join("src"));

    const srcPacks = new PackGroup("./src");

    const start = performance.now();
    let lastReport = start;

    srcPacks.packs.forEach((pack) => {
        if(folders.includes(pack.name)) {
            console.log("Pruning pack:", pack.name);
            const docs = pack.documents;
            docs.forEach((doc, i) => {
                const startCurrent = performance.now();
                // Do actual cleaning (updateDoc calls cleanup routines itself)
                // TODO: Save document only if it has changed.
                pack.updateDoc(doc);

                // Report processing state every few seconds
                const now = performance.now();
                if ((now - lastReport) > 15_000) {
                    lastReport = now;
                    const currentDocTime = now - startCurrent;
                    console.log(i+1, "/", docs.length, ":", doc.name, `[${Math.round(currentDocTime)}ms]`);
                }
            });
        }
    });

    const end = performance.now();
    console.log("All packs processed in", Math.round(end - start), "ms");
}