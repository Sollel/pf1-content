// Remove data that is not needed to be present.

/**
 * Remove unnecessary core Foundry data.
 */
function pruneCoreFoundry(json) {
  // Sorting position
  delete json.sort;

  // Active effects
  if (json.effects?.length === 0) delete json.effects;

  // Flags
  if (json.flags) {
    Object.entries(json.flags).forEach(([flag, value]) => {
      try {
        if (value == undefined || Object.keys(value).length === 0)
          delete json.flags[flag];
      }
      catch (err) {
        console.error(err, { value });
      }
    });
    if (Object.keys(json.flags).length === 0) delete json.flags;
  }

  // _stats info
  delete json._stats;

  // Folder
  if (json.folder?.length === 0 || json.folder == null) delete json.folder;
  // User permissions
  delete json.permission;
}

// Handle PF1 data present in most item types
function pruneCorePF1(json) {
  const data = json.system;

  // Boolean & Dictionary flags
  if (data.flags) {
    // Deal with both arrays and objects. Arrays are no longer supported, however.
    if (data.flags.boolean) {
      if ((Array.isArray(data.flags.boolean) && data.flags.boolean.length == 0) || Object.keys(data.flags.boolean).length == 0)
        delete data.flags.boolean;
    }
    if (data.flags.dictionary) {
      if ((Array.isArray(data.flags.dictionary) && data.flags.dictionary.length == 0) || Object.keys(data.flags.dictionary).length == 0)
        delete data.flags.dictionary;
    }
  }

  // Script calls
  if (data.scriptCalls?.length === 0) delete data.scriptCalls;

  // Associations
  if (data.associations) {
    // Remove empty values
    if (data.associations.classes)
      data.associations.classes = data.associations.classes.filter(i => i?.length > 0);

    // No class associations left
    if (data.associations.classes?.length === 0)
      delete data.associations.classes;

    // No associations left at all
    if (Object.keys(data.associations).length == 0)
      delete data.associations;
  }

  // Links
  if (data.links) {
    if (data.links.charges?.length === 0)
      delete data.links.charges;
    if (data.links.children?.length === 0)
      delete data.links.children;

    // No links left
    if (Object.keys(data.links).length === 0)
      delete data.links;
  }

  // Changes
  if (data.changes?.length === 0)
    delete data.changes;

  if (data.contextNotes?.length === 0)
    delete data.contextNotes;

  if (data.conditionals?.length === 0)
    delete data.conditionals;

  if (data.useCustomTag === false)
    delete data.useCustomTag;

  if (data.tag?.length === 0)
    delete data.tag;

  if (data.showInQuickbar === false)
    delete data.showInQuickbar;

  // Change flags
  if (data.changeFlags) {
    // Remove change flags that are not in effect
    Object.entries(data.changeFlags).forEach(([flag, value]) => {
      if (value === false) delete data.changeFlags[flag];
    });

    // No change flags left?
    if (Object.keys(data.changeFlags).length === 0)
      delete data.changeFlags;
  }

  // Clear empty armor proficiencies
  if (data.armorProf) {
    if (data.armorProf.custom?.length === 0)
      delete data.armorProf.custom;
    if (data.armorProf.value?.length === 0)
      delete data.armorProf.value;
    if (Object.keys(data.armorProf).length === 0)
      delete data.armorProf;
  }

  // Clear empty weapon proficiencies
  if (data.weaponProf) {
    if (data.weaponProf.custom?.length === 0)
      delete data.weaponProf.custom;
    if (data.weaponProf.value?.length === 0)
      delete data.weaponProf.value;
    if (Object.keys(data.weaponProf).length === 0)
      delete data.weaponProf;
  }

  // CR offset
  if (data.crOffset?.length === 0)
    delete data.crOffset;

  // Sound effect
  if (data.soundEffect?.length === 0)
    delete data.soundEffect;

  if (data.disabled === false)
    delete data.disabled;
}

function pruneAction(json) {
  if (json.actionType?.length === 0)
    delete json.actionType;

  if (json.effectNotes?.length === 0)
    delete json.effectNotes;

  if (json.spellFailure !== undefined) {
    // Spell failure is supposed to be a number
    json.spellFailure = Number(json.spellFailure);

    if (json.spellFailure === 0)
      delete json.spellFailure;
  }

  if (json.uses?.per === null) {
    delete json.uses.max;
    delete json.uses.maxFormula;
    delete json.uses.value;
    delete json.uses.per;
  }

  if (json.target) {
    if (json.target.value?.length === 0)
      delete json.target.value;
    if (Object.keys(json.target).length === 0)
      delete json.target;
  }

  if (json.save) {
    if (json.save.description?.length === 0)
      delete json.save.description;
    if (json.save.dc === 0 && json.type !== 'spell')
      delete json.save;
  }

}

function pruneRange(json) {
  const rangeType = json.range?.value;
  if (rangeType?.length === 0 || rangeType === null)
    delete json.range;
}

// Handle attack related data if present.
function pruneAttackLike(json) {
  if (json.ability) {
    if (json.ability.attack === null)
      delete json.ability.attack;
    if (json.ability.damage === null)
      delete json.ability.damage;
    if (json.ability.critRange === 20)
      delete json.ability.critRange;
  }

  if (json.attackName?.length === 0)
    delete json.attackName;
  if (json.attackBonus?.length === 0)
    delete json.attackBonus;
  if (json.critConfirmBonus?.length === 0)
    delete json.critConfirmBonus;

  if (json.attackParts?.length === 0)
    delete json.attackParts;

  if (json.attackNotes?.length === 0)
    delete json.attackNotes;

  if (json.attack) {
    if (json.attack.parts?.length === 0)
      delete json.attack.parts;
  }

  if (json.enh == null)
    delete json.enh;

  const fmAtk = json.formulaicAttacks;
  if (fmAtk) {
    if (fmAtk.count?.formula?.length === 0)
      delete fmAtk.count;
    if (fmAtk.bonus?.formula?.length === 0)
      delete fmAtk.bonus;
    if (fmAtk.label?.length === 0 || fmAtk.label === null)
      delete fmAtk.label;

    if (Object.keys(fmAtk).length === 0)
      delete json.formulaicAttacks;
  }

  if (json.damage) {
    if (json.damage.parts?.length === 0)
      delete json.damage.parts;
    if (json.damage.critParts?.length === 0)
      delete json.damage.critParts;
    if (json.damage.nonCritParts?.length === 0)
      delete json.damage.nonCritParts;

    if (Object.keys(json.damage).length === 0)
      delete json.damage;
  }

  if (json.nonlethal === false)
    delete json.nonlethal;
}

function pruneTemplate(json) {
  const mtpl = json.measureTemplate;
  if (mtpl) {
    if (mtpl.customColor?.length === 0)
      delete mtpl.customColor;
    if (mtpl.customTexture?.length === 0)
      delete mtpl.customTexture;
    if (mtpl.size?.length === 0)
      delete mtpl.size;
    if (mtpl.type?.length === 0)
      delete mtpl.type;
    if (mtpl.overrideColor === false)
      delete mtpl.overrideColor;
    if (mtpl.overrideTexture === false)
      delete mtpl.overrideTexture;

    if (Object.keys(mtpl).length === 0)
      delete json.measureTemplate;
  }
}

function pruneFeature(json) {
  if (json.abilityType === "none" || json.abilityType == null)
    delete json.abilityType;
}

function pruneEquipment(json) {
  if (json.subType !== 'misc')
    delete json.slot;
}

function pruneAttack(json) {
  if (json.naturalAttack?.primaryAttack === true || json.subType !== 'natural')
    delete json.naturalAttack.primaryAttack;
}

// Delete data that hasn't been used in a long long time by the system
function deleteOldItemDataCruft(json) {
  const data = json.system;

  if (data.recharge)
    delete data.recharge;
  if (data.damageType)
    delete data.damageType;
  if (data.formula?.length === 0)
    delete data.formula;
  if (data.time)
    delete data.time;
}

function pruneItem(json) {
  pruneCorePF1(json);
  pruneAttackLike(json);
  pruneAction(json);
  pruneRange(json);
  pruneTemplate(json);

  switch (json.type) {
    case 'feat':
      pruneFeature(json);
      break;
    case 'equipment':
      pruneEquipment(json);
      break;
    case 'attack':
      pruneAttack(json);
  }

  deleteOldItemDataCruft(json);
}

function pruneActor(json) {
  if (json.items?.length === 0)
    delete json.items;
}

export default function pruneDocument(json, options) {
  pruneCoreFoundry(json, options);

  const type = options.type;

  // WARNING: The tooling does not distinguish pack document types, so the following is a bit risky.
  // Following checks are to minimize errors, but they're not foolproof.

  // Journals, untyped documents, etc. should not be processed farther.
  if (!type || !json.system || !json.type) return;

  switch (type) {
    case 'Actor': return pruneActor(json, options);
    case 'Item': return pruneItem(json, options);
  }
}
